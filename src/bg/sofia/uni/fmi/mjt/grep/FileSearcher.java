package bg.sofia.uni.fmi.mjt.grep;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Formatter;

public class FileSearcher {
	String path;
	String stringToFind;
	StringBuffer buffer;
	Formatter formatter;
	GrepOptions opt;

	public FileSearcher(String path, String stringToFind) {
		this.path = path;
		this.stringToFind = stringToFind;
		buffer = new StringBuffer();
		opt = GrepOptions.NO_OPTION;
		formatter = new Formatter(buffer);
	}

	public FileSearcher(GrepOptions opt, String path, String stringToFind) {
		this.path = path;
		this.stringToFind = stringToFind;
		buffer = new StringBuffer();
		this.opt = opt;
		formatter = new Formatter(buffer);
	}

	public StringBuffer searchForStringInFile() {
		try (BufferedReader buffReader = new BufferedReader(new FileReader(path))) {
			int numberOfLine = 0;
			String line = buffReader.readLine();
			while (line != null) {

				switch (opt) {
				case W:
					if (searchForWholeWordInLine(line, numberOfLine)) {
						numberOfLine++;
					}
					break;
				case I:
					if (searchForStringInLineKeyInsensitive(line, numberOfLine)) {
						numberOfLine++;
					}
					break;
				case WI:
					if (searchForWholeWordInLineKeyInsensitive(line, numberOfLine)) {
						numberOfLine++;
					}
					break;
				default:
					if (searchForStringInLine(line, numberOfLine)) {
						numberOfLine++;
					}
				}
				line = buffReader.readLine();
			}
			return buffer;
		} catch (FileNotFoundException e) {
			System.out.println("File: " + path + "was not found");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return buffer;
	}

	private boolean searchForWholeWordInLine(String line, int numberOfCurrentLine) {
		boolean found = Arrays.asList(line.split("[^a-zA-Z0-9]+")).contains(stringToFind);
		if (found) {
			formatter.format("%1$s:%2$s:%3$s%n", path, numberOfCurrentLine, line);
		}
		return found;
	}

	private boolean searchForWholeWordInLineKeyInsensitive(String line, int numberOfCurrentLine) {
		boolean found = Arrays.asList(line.toLowerCase().split("[^a-zA-Z0-9]+")).contains(stringToFind.toLowerCase());
		if (found) {
			formatter.format("%1$s:%2$s:%3$s%n", path, numberOfCurrentLine, line);
		}
		return found;
	}

	private boolean searchForStringInLineKeyInsensitive(String line, int numberOfCurrentLine) {
		boolean found = line.toLowerCase().contains(stringToFind.toLowerCase());
		if (found) {
			formatter.format("%1$s:%2$s:%3$s%n", path, numberOfCurrentLine, line);
		}
		return found;
	}

	private boolean searchForStringInLine(String line, int numberOfCurrentLine) {
		boolean found = line.contains(stringToFind);
		if (found) {
			formatter.format("%1$s:%2$s:%3$s%n", path, numberOfCurrentLine, line);
		}
		return found;
	}
}
