package bg.sofia.uni.fmi.mjt.grep;

public enum GrepOptions {
	W, I, WI, NO_OPTION
}
