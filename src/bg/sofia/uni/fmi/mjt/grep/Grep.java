package bg.sofia.uni.fmi.mjt.grep;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.AccessDeniedException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Grep {
	ExecutorService exec;

	public Grep(int numberOfParallelThreads) {
		exec = Executors.newFixedThreadPool(numberOfParallelThreads);
	}

	public void grep(String... args) {
		switch (args[1]) {
		case "-wi":
			if (args.length == 6) {
				grepWithOptionsToFile(GrepOptions.WI, args[2], args[3], args[5]);
			} else {
				grepWithOptionsToConsole(GrepOptions.WI, args[2], args[3]);
			}
			;
			break;
		case "-w":
			if (args.length == 6) {
				grepWithOptionsToFile(GrepOptions.W, args[2], args[3], args[5]);
			} else {
				grepWithOptionsToConsole(GrepOptions.W, args[2], args[3]);
			}
			;
			break;
		case "-i":
			if (args.length == 6) {
				grepWithOptionsToFile(GrepOptions.I, args[2], args[3], args[5]);
			} else {
				grepWithOptionsToConsole(GrepOptions.I, args[2], args[3]);
			}
			;
			break;
		default:
			if (args.length == 6) {
				grepNoOptionsToFile( args[1], args[2], args[4]);
			} else {
				grepNoOptionsToConsole(args[1], args[2]);
			}
			;
			break;

		}
	}

	private void printStringOccurenceInfo(GrepOptions option, File searchIn, String stringToFind, File writeTo) {
		try {
			canWriteToFile(writeTo);
		} catch (AccessDeniedException e1) {
			System.out.println("Can't write in: " + writeTo.getPath());
		}
		FileSearcher searcher;
		if (option.equals(GrepOptions.NO_OPTION)) {
			searcher = new FileSearcher(searchIn.getPath(), stringToFind);
		} else {
			searcher = new FileSearcher(option, searchIn.getPath(), stringToFind);
		}
		Runnable runnableTask = () -> {
			try {
				printTheResult(searcher, writeTo);
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		exec.submit(runnableTask);
	}

	private void printTheResult(FileSearcher searcher, File writeTo) throws IOException {
		OutputStream stream;
		if (writeTo == null) {
			stream = System.out;
			writeToConsole(stream, searcher);
			//stream.close();
		} else {
			try {
				stream = new FileOutputStream(writeTo, true);
				writeToFile(stream, searcher);
				//stream.close();
			} catch (FileNotFoundException e) {
				System.out.println("File: " + writeTo.getPath() + "was not found");
			}
		}
	}

	private void writeToConsole(OutputStream stream, FileSearcher searcher) {
		try {
			synchronized (stream) {
				stream.write(String.valueOf(searcher.searchForStringInFile()).getBytes());
				stream.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeToFile(OutputStream stream, FileSearcher searcher) {
		synchronized (stream) {
			try {
				stream.write(String.valueOf(searcher.searchForStringInFile()).getBytes());
				stream.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void canWriteToFile(File file) throws AccessDeniedException {
		if (file != null && !file.canWrite()) {
			throw new AccessDeniedException(file.getPath());
		}
	}

	private void grepNoOptionsToConsole(String stringToFind, String pathToDirectoryTree) {
		File file = new File(pathToDirectoryTree);
		if (file.isDirectory()) {
			File[] filesInCurrentDirectory = file.listFiles();
			for (File f : filesInCurrentDirectory) {
				grepNoOptionsToConsole(stringToFind, f.getPath());
			}
		} else {
			printStringOccurenceInfo(GrepOptions.NO_OPTION, file, stringToFind, null);
		}
	}

	private void grepWithOptionsToConsole(GrepOptions opt, String stringToFind, String pathToDirectoryTree) {
		File file = new File(pathToDirectoryTree);
		if (file.isDirectory()) {
			File[] filesInCurrentDirectory = file.listFiles();
			for (File f : filesInCurrentDirectory) {
				grepWithOptionsToConsole(opt, stringToFind, f.getPath());
			}
		} else {
			printStringOccurenceInfo(opt, file, stringToFind, null);
		}
	}

	private void grepWithOptionsToFile(GrepOptions opt, String stringToFind, String pathToDirectoryTree,
			String pathToOutputFile) {
		File file = new File(pathToDirectoryTree);
		if (file.isDirectory()) {
			File[] filesInCurrentDirectory = file.listFiles();
			for (File f : filesInCurrentDirectory) {
				grepWithOptionsToFile(opt, stringToFind, f.getPath(), pathToOutputFile);
			}
		} else {
			printStringOccurenceInfo(opt, file, stringToFind, new File(pathToOutputFile));
		}
	}

	private void grepNoOptionsToFile(String stringToFind, String pathToDirectoryTree, String pathToOutputFile) {
		File file = new File(pathToDirectoryTree);
		if (file.isDirectory()) {
			File[] filesInCurrentDirectory = file.listFiles();
			for (File f : filesInCurrentDirectory) {
				grepNoOptionsToFile(stringToFind, f.getPath(), pathToOutputFile);
			}
		} else {
			printStringOccurenceInfo(GrepOptions.NO_OPTION, file, stringToFind, new File(pathToOutputFile));
		}
	}

}
