package bg.sofia.uni.fmi.mjt.grep;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		String grepCommand;
		Scanner in = new Scanner(System.in); 
		grepCommand = in.nextLine(); 
		String[] commandParts = grepCommand.split(" ");
		Grep g;
		if(commandParts[1].equals("-w") || commandParts[1].equals("-i") || commandParts[1].equals("-wi")) {
			g = new Grep(Integer.parseInt(commandParts[4]));
		}else {
			g = new Grep(Integer.parseInt(commandParts[3]));
		}
		g.grep(commandParts);
		in.close();
	}
}